var SC = require('../sc');
var optimist = require('optimist');
var Color = require('../libs/color');

var argv = optimist.usage('Usage: cli.js -i input_path -o output_path [options]')
	.demand(['i','o'])
	.alias('i','input').describe('i','Path of the input image')
	.alias('o','output').describe('o','Path of the output image')
	.alias('w','width').describe('w','Output width')
	.alias('h','height').describe('h','Output height')
	.alias('m','midterm').describe('m','Path of the mid-term image (After image filtering). PNG Format only.')
	.alias('c','colors').describe('c','Colors that will be used in photo filtering. At least 3 colors')
	.argv;


if(argv.i && argv.i.length && argv.o && argv.o.length){
	var options = {};

	if(argv.c ){
		var colors ;
		if(typeof argv.c == 'string'){
			colors = argv.c.split(',');
		}else{
			colors = argv.c;
		}

		var palcolors = [];
		colors.forEach(function(colorStr){
			palcolors.push( colorStr );
		})

		if(palcolors.length < 3){
			throw new Error('At least 3 colors in pattle.');
		}
		options.colors = palcolors;
	}

	if(argv.width){
		options.width = parseInt(argv.width);
	}
	if(argv.height){
		options.height = parseInt(argv.height);
	}
	if(argv.midterm){
		options.midterm = (argv.midterm);
	}
	SC.convert(argv.i, argv.o, options);
}


