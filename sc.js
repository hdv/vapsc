"use strict";

var Color = require('./libs/color')
  , ImageTracer = require('./libs/imagetracer')
  , PImage = require('pureimage')
  , fs = require('fs')
  , tmp = require('tmp')
  , path = require('path')
  , Canvas = require('canvas')
  , Image = Canvas.Image
  , Caman = require('caman').Caman
  , svg2png = require('svg2png')
;

const readChunk = require('read-chunk'); // npm install read-chunk 
const imageType = require('image-type');



// Default colors
var DefaultColors = [
	0xFFD700, 0xDAA520,
	0x000080, 
	0x0000FF, 0x00BFFF, 0xB0E0E6, 0x32CD32, 0x228B22, 0x2E8B57,
	0x808080,
	0xFFFFFF,
	0x8B0000, 0xFF8C00, 0xFFC0CB, 0xFF00FF, 0xFFFF00
];

var params = {};
var DefaultFilters = {
	contrast: 25,
	saturation: 48,
	exposure: 10,
	vibrance: 22,
	clip: 45,
	brightness: 5,
	hue:0,
	stackBlur:2,
	sharpen: 0,
};



var fixRange = function(v, o, r = 20 ){
	if(o == 0) return v;

	v = v + o * r;
	if( v> 255) throw new Error('Too large');
	if( v< 0) throw new Error('Too small');
	return v;
}

function initColors ( options){
	var colors = options.colors || DefaultColors;
	// Initialize pal colors in object
	var palcolors = [];
	colors.forEach(function(color){

		var _color = null;
		if(typeof color == 'string'){
			_color =  Color.fromHex(  color.substr(0,1) == '#' ?color.substr(1) : color);
		}
		else if(typeof color == 'number')
			_color = Color.fromDigit(color);
		else
			throw new Error('ColorFormatNotAccepted');

		_color.a = 255;
		try{
			palcolors.push(_color);

		}catch(err){
			console.error(err);
		}
	});

	return palcolors;
}

function readFile(filepath){
	return new Promise (function(resolve, reject){

		var buffer = readChunk.sync(filepath, 0, 12);

		var imageFileType = imageType(buffer);

		if( imageFileType.ext != 'png' && imageFileType.ext != 'jpg'){
			console.error('UnsupportedFileFormatError', imageFileType);
			reject({type:'UnsupportedFileFormatError'});
			return;
		}

		resolve({filepath: filepath});

		// .catch(function(err){
		// 	console.error('[SC.readFile] Cannot read file at ' + filepath, err);
		// 	reject({type:'FileIOError'});
		// });// End of fs.readFile()
	});
}

function bufferToJpeg(width, height, buffer, outputpath, onComplete)
{


	var img = new Image;
	img.onload = function(){
		
		var canvas = new Canvas(width, height);
		canvas.getContext('2d').drawImage(img, 0,0,width, height);

		var out = fs.createWriteStream(outputpath);

		var stream = canvas.jpegStream();

		stream.on('data', function(chunk){
			out.write(chunk)
		})

		stream.on('end', function(){
			out.end();
			console.error('[SC/writeFile] Wrote file at ' + outputpath);
			onComplete();
		})
	}
	img.src = buffer;
}
function bufferToPng(width, height, buffer, outputpath, onComplete)
{


	var img = new Image;
	img.onload = function(){
		
		var canvas = new Canvas(width, height);
		canvas.getContext('2d').drawImage(img, 0,0,width, height);

		var out = fs.createWriteStream(outputpath);

		var stream = canvas.pngStream();

		stream.on('data', function(chunk){
			out.write(chunk)
		})

		stream.on('end', function(){
			out.end();
			console.error('[SC/writeFile] Wrote file at ' + outputpath);
			onComplete();
		})
	}
	img.src = buffer;
}

function applyStyle ( result, params, filters, midpath ){

	return new Promise( function (resolve, reject){

		Caman(result.canvas.toBuffer() , function(){
			console.log('[SC/applyStyle] Applying filter start', filters);

			// Applying filters property into Caman object
			var filterKeys = Object.keys(filters);
			var self = this;
			filterKeys.forEach(function(keyName){
				console.log('[SC/applyStyle] f.'+keyName+'=', filters[keyName])
				self[keyName]( filters[keyName] );
			})

			// Executing filter on the canvas
			this.render(function(){
				console.log('[SC/applyStyle] Applying filter end')

				// Generate mid-term image buffer 
				var buffer = this.canvas.toBuffer();

				if( midpath ){

					bufferToPng(result.canvas.width, result.canvas.height, buffer, midpath, function(){

						console.log('[SC/applyStyle] Save mid-term image at ',midpath);					
					} );
				}



				var img = new Image;
				img.onload = function(){

					console.log('[SC/applyStyle] Generating svg content started')

					var canvas = new Canvas(img.width, img.height);
					var ctx = canvas.getContext('2d');
					ctx.drawImage( img, 0, 0, img.width, img.height);

					// Getting ImageData from canvas with the helper function getImgdata().
					var imageData = ImageTracer.getImgdata(canvas);
					
					// Synchronous tracing to tracedata
					// var tracedata = ImageTracer.imagedataToTracedata( imgd, options );
					// resolve(canvas , tracedata);


					var svgdata = ImageTracer.imagedataToSVG( imageData,  params);
					console.log('[SC/applyStyle] Generating svg content completed')

					resolve({output: {width: imageData.width, height: imageData.height, data: svgdata, type:'svg'}});


				}
				img.onerror = function(){
					console.log('[SC/applyStyle] Cannot load temporary file at ', img.src);
					reject({type:'InvalidImageContentError'})
				}
				img.src = buffer;

			});
		})

		//*/
	})
}

// Initialize image object for next step
function initImage (result, dwidth, dheight)
{
	return new Promise(function(resolve, reject){

		var img = new Image;
		img.onload = function(){
			console.log('[SC/initImage] Source Image Loaded');

			var canvas = new Canvas( dwidth, dheight);
			var ctx = canvas.getContext('2d');

			var scale = 1 ;
			var sx = 0;
			var sy = 0;

			scale = dwidth / img.width ;
			// if( srcImage.width * scale > canvas.width){
			// 	scale = canvas.width / img.width ;
			// }
			if( img.height * scale < dheight){
				scale = dheight / img.height ;
			}
			var sw = img.width * scale;
			var sh = img.height * scale;
			sx = dwidth *.5  - sw * .5;
			sy = dheight *.5  - sh * .5;

			ctx.drawImage(img, sx, sy, sw, sh);

			console.log('[SC/initImage] Cropped image');
			// Send back caller
			resolve({ canvas: canvas });

		}
		img.onerror = function(err){

			console.error('[SC/initImage] Cannot read the unknown data from input file', result.filepath);
			console.error(err);
			reject({type:'UnableReadFileError', error:err});
		}
		img.src = result.filepath;

	})
}

// Final output into a file
function writeFile(outputpath, result)
{
	return new Promise(function(resolve, reject){


		var ext = path.extname(outputpath);
		var renderFuncName = '';

		if(ext == '.png'){
			renderFuncName = 'png';

		}else if(ext == '.jpg' || ext == '.jpeg') {
			renderFuncName = 'jpeg';

		}else if(ext == '.svg'){
			fs.writeFile(outputpath, result.output.data, (err) =>{
				if( err){

					console.error('[SC/writeFile] Cannot write temporary svg file',err);
					reject({type:'IOError', error: err});
					return;
				}

				console.error('[SC/writeFile] Wrote svg file at ',outputpath);
				resolve({path: outputpath});


			})

			return;
		}

		if(!renderFuncName ){
			console.error('UnsupportedFileFormatError', ext);
			reject({type:'UnsupportedFileFormatError'});
			return;
		}


		var tmpobj = tmp.fileSync ({prefix:'sc3-',postfix:'.svg'});

		//console.log('[SC/applyStyle] Write into tmp file ', tmpobj.name);
		fs.writeFile(tmpobj.name, result.output.data, (err) =>{
			if( err){

				tmpobj.removeCallback();

				console.error('[SC/writeFile] Cannot write temporary svg file',err);
				reject({type:'IOError', error: err});
				return;
			}

			//console.log('[SC/applyStyle] Read from tmp file ', tmpobj.name);
			fs.readFile(tmpobj.name, function(err, buffer){
				if( err){

					tmpobj.removeCallback();

					console.error('[SC/writeFile] Cannot read temporary svg file',err);
					reject({type:'IOError', error: err});
					return;
				}

				var outputBuffer;
				try{
					console.log('[SC/writeFile] Converting into image data.', buffer);
					outputBuffer = svg2png.sync( buffer);
				}catch(err){
					tmpobj.removeCallback();
					console.log('[SC/writeFile] Error when converting into image data.', err);
					reject({type:'ConvertError',error: err});
					return;
				}


				console.log('[SC/writeFile] Converted to image data.', outputBuffer);

				// var data = {width: img.width, height: img.height, data: outputBuffer};

				tmpobj.removeCallback();

				var onComplete= function(){
					resolve({path: outputpath});
				}

				if(renderFuncName == 'jpeg')
					bufferToJpeg(result.output.width, result.output.height, outputBuffer, outputpath, onComplete)
				if(renderFuncName == 'png')
					bufferToPng(result.output.width, result.output.height, outputBuffer, outputpath, onComplete)

			});

		})

	})
}

/**
* Start converting of a image
*
* @param {string} Input - Input file path. Support jpeg, png formats.
* @param {string} Output - Output file path. Support jpeg, png and svg formats.
* @param {object} Options of processor. Optional parameter.
* @return {Proimse} Promising callback for getting result in async way.
*/ 
function convert(inputfile, outputfile, options){
    console.log('[SC.version='+SC.VERSION+']');
	return new Promise(function(resolve, reject){


		console.log('[SC.process] options:' ,options);

		var dwidth = options.width || 1000;
		var dheight = options.height || 1000;

		
		// Load file from input
		var palcolors = initColors(options);

		var presetName = options.presetName || 'Randomsampling1';
		var preset = ImageTracer.optionpresets[presetName];
		
		var keys = Object.keys(preset);
		keys.forEach(function(key){
			params[key] = preset[key];
		})
		
		if(typeof options.params != 'undefined'){
		    keys = Object.keys(options.params);
		    keys.forEach(function(key){
			    params[key] = options.params[key];
		    })
		}
		
		params.colorsampling = 0;
		params.numberofcolors = palcolors.length;
		params.pal = palcolors;

		if(typeof params.colorquantcycles == 'undefined') params.colorquantcycles = 1;
		if(typeof params.blurradius == 'undefined')params.blurradius = 0;
		if(typeof params.blurdelta == 'undefined')params.blurdelta = 50;
		if(typeof params.scale == 'undefined')params.scale = 1;
		// options.strokewidth = 0;
		if(typeof params.linefilter == 'undefined')params.linefilter = true;
		if(typeof params.pathomit == 'undefined')params.pathomit = 20;
        

		console.log('[SC.process] size:' ,dwidth, dheight);
		console.log('[SC.process] params:' ,params);

		var inputpath = path.resolve(inputfile);
		var outputpath = path.resolve(outputfile);
		var midtermpath = options.midterm ? path.resolve(options.midterm) : null;

		var filters = options.filters || DefaultFilters;

		readFile(inputpath)
			.catch(function(err){
				console.error(err);
				console.error('[SC.process] Cannot read file at ' + inputpath);
				reject(err);
				//throw new Error(err.type);
			})
			.then(function(result){
				initImage(result, dwidth, dheight)
					.then(function(result){
						applyStyle(result, params, filters, midtermpath)

							.then(function(result){
								writeFile(outputpath, result)
								    .catch(function(err){
							            console.error('[SC.process/writeFile] Error ',err);
                        				reject(err);
                        				//throw new Error(err.type);
								    })
									.then(function(result){
										resolve(result);
									});
							})
							.catch(function(err){
								console.error(err);
								console.error('[SC.process/applyStyle] Error ',err);
                				reject(err);
                				//throw new Error(err.type);
							})
					})
					.catch(function(err){
						console.error(err);
						console.error('[SC.process/initImage] Error ',err);
    				reject(err);
    				//throw new Error(err.type);
					})
			})


	});
}

var SC = {
    BUILD: '20170728001',
    VERSION: '0.0.3',
    // Link up with library as tool for external users.
    Color: Color,
    ImageTracer: ImageTracer,
    Canvas: Canvas,
	defaultColors: function(){
		return DefaultColors;
	},
	defaultFilters: function(){
		return DefaultFilters;
	},

	convert: convert
}

module.exports = SC;
