(function(context) {

	function d2h(d) {return d.toString(16);}
	function h2d(h) {return parseInt(h,16);}
	function fillZero(h, length){
		while(h.length< length){
			h = '0'+h;
		}
		return h;
	}
	function h2rgb(h){
		h = fillZero(h,6);
		return {r: parseInt(h.substr(0,2),16), g:parseInt(h.substr(2,2),16), b:parseInt(h.substr(4,2),16) };
	}
	function cssRgba2o(str) { 
		var regex = /rgba\s*\(\s*([0-9]+),\s*([0-9]+),\s*([0-9]+).*\)/g;
		var matches = regex.exec(str);
		if(!matches || matches.length <=3){
			throw new Error('Invalid rgba() format');
		}
		return {r: parseInt(matches[1]), g: parseInt(matches[2]), b:parseInt(matches[3])};
	};
	function rgb2Css(rgb,alpha){
		if(typeof rgb != 'object')
			throw new Error('Passed first argument is not an object');
		if(typeof alpha == 'undefined') alpha  = 1;
		return 'rgba('+[rgb.r,rgb.g,rgb.b,alpha].join(',')+')';
	}
	function rgb2Hex(rgb,alpha){
		if(typeof rgb != 'object')
			throw new Error('Passed first argument is not an object');
		return fillZero(d2h(rgb.r),2)+fillZero(d2h(rgb.g),2)+fillZero(d2h(rgb.b),2);
	}
	function rgb2CssHex(rgb,alpha){
		return '#'+rgb2Hex(rgb,alpha);
	}

	var ColorInfo = function(){}
	ColorInfo.prototype = {
		r:0,
		g:0,
		b:0,
		value:0x0,
		text:'',
		hex:'',
		fromCssRgba:function(str){
			return this.fromRgb(cssRgba2o(str));
		},
		fromRgb: function(r,g,b){
			if(typeof r == 'object'){
				this.r = r.r;
				this.g = r.g;
				this.b = r.b;
			}else{
				this.r = r;
				this.g = g;
				this.b = b;
			}

			this.text = rgb2Hex(this);
			this.hex = this.text;
			this.value = h2d(this.text);

			return this;
		},

		fromHex: function(h){
			return this.fromRgb( h2rgb( h ));
		},
		fromDigit: function(d){
			return this.fromHex(d2h(d));
		},
		toHex: function(){
			return this.text;
		},
		toBrightness: function(){
			return (0.2126*this.r + 0.7152*this.g + 0.0722*this.b) << 0
		},
		toGrayscale: function(){
			return (1/3*this.r + 1/3*this.g + 1/3*this.b)<< 0
		},
		toCssHex: function(){
			return '#'+this.text;
		},
		toCssRgb: function(){
			return rgb2Css(this);
		},
		toRgb: function(){
			return {r: this.r, g:this.g, b: this.b };
		},
		toDigit: function(){
			return this.value;
		}
	};

	var g = {};
	g.fromDigit = function(){
		var c = new ColorInfo;
		return c.fromDigit.apply(c, arguments);
	};
	g.fromHex = function(){
		var c = new ColorInfo;
		return c.fromHex.apply(c, arguments);
	};
	g.fromRgb = function(){
		var c = new ColorInfo;
		return c.fromRgb.apply(c, arguments);
	};
	g.fromCssRgba = function(){
		var c = new ColorInfo;
		return c.fromCssRgba.apply(c, arguments);
	}
	g.digitToBrightness = function(val)
	{
		var c = new ColorInfo;
		return c.fromDigit(val).toBrightness()
	}
	g.hexToBrightness = function(val)
	{
		var c = new ColorInfo;
		return c.fromHex(val).toBrightness()
	}

	g.VERSION = '0.5.0';

	if (typeof module === 'object' && module && module.exports) {
		module.exports = g;
	} else {
		context.Color = g;
	}

})(this);